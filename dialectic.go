package dialectic

import "time"

// User contains information about a user
type User struct {
	ID             int
	Name           string
	Email          string
	HashedPassword string
	BlockMode      bool
	AllowMode      bool
	VotesGiven     int
	VotesRecieved  int
	ShowHidden     bool
	LastLogin      time.Time
	CreatedOn      time.Time
	HiddenTo       time.Time
	Banned         bool
}

// UserService is for manipulating users
type UserService interface {
	Login(nameOrEmail string, password string) (user *User, err error)
	SignUp(name string, email string, password string) (user *User, err error)
	BlockTopics([]*Topic) (err error)
	UnblockTopics([]*Topic) (err error)
	AllowTopics([]*Topic) (err error)
	UnallowTopics([]*Topic) (err error)
}

// Topic contains information about a topic
type Topic struct {
	ID             int
	Name           string
	Extract        string
	WikipediaID    int
	DefaultBlocked bool
	DefaultAllowed bool
	CreatedOn      time.Time
}

// TopicService is for manipulating topics
type TopicService interface {
	NewTopic(page *WikiPage) (topic *Topic, err error)
	TopicsByName(topicNames []string) (topics []*Topic, err error)

	HomeExpDecayArticles(user *User, halfLife int, limit int, offset int) (articles []*Article, err error)
	HomeNewestArticles(user *User, limit int, offset int) (articles []*Article, err error)
	HomeMostVotedArticles(user *User, timePeriod string, limit int, offset int) (articles []*Article, err error)

	TopicsExpDecayArticles(topics []*Topic, halfLife int, limit int, offset int) (articles []*Article, err error)
	TopicsNewestArticles(topics []*Topic, limit int, offset int) (articles []*Article, err error)
	TopicsMostVotedArticles(topics []*Topic, timePeriod string, limit int, offset int) (articles []*Article, err error)

	UserTopicsExpDecayArticles(user *User, topics []*Topic, halfLife int, limit int, offset int) (articles []*Article, err error)
	UserTopicsNewestArticles(user *User, topics []*Topic, limit int, offset int) (articles []*Article, err error)
	UserTopicsMostVotedArticles(user *User, topics []*Topic, timePeriod string, limit int, offset int) (articles []*Article, err error)
}

// Post contains information about a post (article or comment)
type Post struct {
	ID        int
	UserName  string
	Content   int
	Votes     int
	Views     int
	CreatedOn time.Time
	ActiveTo  time.Time
	Comments  []*Comment
}

// PostService is for manipulating posts
type PostService interface {
	Vote(*User) (err error)
	View(*User) (err error)
	Block(*User) (err error)
	Save(*User) (err error)
	Comment(*Comment) (err error)
	GetVotes() (votes int)
	GetViews() (views int)
}

// Article contains information about a article
type Article struct {
	Post
	Title      string
	TopicNames []string
}

// ArticleService is for manipulating articles
type ArticleService interface {
	PostService
	GetTopicVotes(*Topic) (votes int, err error)
	UserAddedTopic(*User, *Topic) (err error)
	UserRemovedTopic(*User, *Topic) (err error)
}

// Comment contains information about a comment
type Comment struct {
	Post
	Article *Article
	Parent  *Comment
}

// WikiPage contains information about a Wikipedia page
type WikiPage struct {
	WikipediaID int
	Title       string
	Extract     string
	TotalViews  int
	Namespace   int
	Categories  []string
}

// WikiPageService is for working with Wikipedia pages fetched from Wikipedia's API
type WikiPageService interface {
	Search(query string) (pages []*WikiPage, err error)
	Page(id int) (page *WikiPage, err error)
	Banned(page *WikiPage) (isBanned bool, banMessages []string)
}

// Requestor request resource from the internet
type Requestor interface {
	GetJSON(url string, target interface{}) (err error)
}
