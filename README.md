# Dialectic
A free/libre (as in freedom), open source reddit-like site with a focus on
discussion. This site tries to fix some of the problems with Reddit like the
echo chamber effect and the overreaching of moderator power to shut down
discussion.

### Topic based system
It uses a topic system instead of subreddits. A topic system means that every
post is tagged with some topics decided by the author of the post and the
community viewing the post. This ensures that when you are viewing the topic
"Politics", you will get every political post containing both left-wing and
right-wing politics from the whole world. You could of course also view the
topic "Left-wing politics" or "Politics of the United States" to narrow down
the posts. The available topics are all of the topics with a popular
Wikipedia page about it.

### Anti-authoritarian moderation
This site is anti-authoritarian and does not have any kind of privileged
users with moderation power. Every user has an equally amount of moderation
power and any form of moderation is based upon majority consensus. Every user
has a blacklist or whitelist to block or allow topics in their feed and every
user is allowed to request that a topic is added or removed from a post. The
topics a post has is then decided by the largest amount of common agreement.
There is a upvote system but no down vote button which has been decide to
foster discussion of different viewpoints and not make the majority viewpoint
the only viewpoint. The comments section has a tree-like structure like on
Reddit and every comment is required to be on topic to the post it is
responding to.

### No advertisements
This site will never contain any kind of advertisement. Advertisement steals
your personal data and tries to deceive you into buying their shit. When a
website has advertisement the users attention becomes the product and the
website is incentivized into making the website as addicting and time
consuming as possible to maximize profit at the expense of the users. This
site will be crowd-funded through donations.

### Markdown posts
This site will only have markdown formatted text posts to ensure that the
site is primarily focused on active discussion. You can link to an external
article, image or video inside the text post which can then be optionally
fetched from their hosting website and shown inside the text post.

### Fast site with minimal JavaScript
This site works without JavaScript. Some ease of use features will be added
using TypeScript compiled to JavaScript but these features are completely
optional and not required to use the site. The site is written in Go and
tries to have as few dependencies as possible to ensure that it stays fast.

## TODO
- [ ] Design database
- [ ] Create Go backend with basic HTML pages
- [ ] Add CSS styling
- [ ] Add an REST API and interactive JavaScript features
- [ ] Host the website
- [ ] Use user specified topics when filtering articles
- [ ] Add database caching on the Go backend
- [ ] Optimize database
- [ ] Implement dialectic gold with GNU Taler
- [ ] Research the ActivityPub protocol for federated server support

### Go backend and frontend
Use the libraries blackfriday for markdown and bluemonday for input validation. 
Mathjax can be used on the frontend for latex math. Use gruvbox as the color theme for
the site and Fantasque Sans Mono as the font. Use go bigcache for caching. Make
it possible to view hotlinked pictures/videos after the user click a button and
make every post hide/show when clicking on the title.

### Optimize database
Partioning by month on article and comment table and make a article inactive
after a time period of no new interactions making it impossible to make new
comments or view/upvote article or comments on article. This has the benefit
of making it possible to delete old rows in view_article and view_comments
which point to an inactive article/comment and make it faster to find comments
on an article because the searching for comments is only done in the active
period of the article which benefits from the partioning of the comment
table. The partions of article can be clustered on votes index after some
time of less to none activity on the articles in the partion. The partions 
of comments can also be clustered on article_id and votes index.

### Dialectic gold
Dialectic gold works much like reddit gold. A dialectic gold is worth 1 EUR
and can be given to any post (article or comment). They is a 50% transaction
fee (rounded up) on each gift, where the giver has to donate the transaction
fee to a non-profit charity of his/hers choicing from a selected list of
charities. The reciver gets the rest of the gold and can choice which post
he/she wants to give gold to. Donations are listed on the user page for all
to see.