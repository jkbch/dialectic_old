CREATE TABLE users (
	id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	name VARCHAR ( 50 ) UNIQUE NOT NULL,
	email VARCHAR ( 255 ),
	hashed_password VARCHAR ( 255 ) NOT NULL,
    block_mode BOOLEAN NOT NULL DEFAULT TRUE,
    allow_mode BOOLEAN NOT NULL DEFAULT FALSE,
    votes_given INT NOT NULL DEFAULT 0,
    votes_recieved INT NOT NULL DEFAULT 0,
    show_hidden BOOLEAN NOT NULL DEFAULT FALSE,
    last_login TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
	created_on TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    hidden_to TIMESTAMP WITH TIME ZONE DEFAULT NULL,
    banned BOOLEAN NOT NULL DEFAULT FALSE,
    article_buffer TEXT DEFAULT NULL,
    comment_buffer TEXT DEFAULT NULL
);
CREATE UNIQUE INDEX users_lower_name_idx ON users(LOWER(name));

CREATE TABLE topic (
	id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	name VARCHAR ( 255 ) UNIQUE NOT NULL,
    extract TEXT NOT NULL,
    wikipedia_id INT UNIQUE NOT NULL,
    default_blocked BOOLEAN NOT NULL DEFAULT FALSE,
    default_allowed BOOLEAN NOT NULL DEFAULT FALSE,
	created_on TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);
CREATE UNIQUE INDEX topic_lower_name_idx ON topic(LOWER(name));

CREATE TABLE article (
	id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    users_id INT REFERENCES users(id) ON DELETE SET NULL,
    users_name VARCHAR ( 50 ) REFERENCES users(name) ON UPDATE CASCADE ON DELETE SET NULL,
	title VARCHAR ( 255 ),
    body TEXT NOT NULL,
    votes INT NOT NULL DEFAULT 1,
    views INT NOT NULL DEFAULT 1,
    hidden BOOLEAN NOT NULL DEFAULT FALSE,
	created_on TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    archived BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE comment (
	id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    users_id INT REFERENCES users(id) ON DELETE SET NULL,
    users_name VARCHAR (50) REFERENCES users(name) ON UPDATE CASCADE ON DELETE SET NULL,
    article_id INT NOT NULL REFERENCES article(id) ON DELETE CASCADE,
    parent_comment_id INT REFERENCES comment(id) ON DELETE CASCADE,
    body TEXT NOT NULL,
    votes INT NOT NULL DEFAULT 1,
    views INT NOT NULL DEFAULT 1,
    hidden BOOLEAN NOT NULL DEFAULT FALSE,
	created_on TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);

CREATE TABLE edit_article (
    article_id INT NOT NULL REFERENCES article(id) ON DELETE CASCADE,
    markdown TEXT NOT NULL,
    edited BOOLEAN DEFAULT FALSE
);

CREATE TABLE edit_comment (
    article_id INT NOT NULL REFERENCES article(id) ON DELETE CASCADE,
    markdown TEXT NOT NULL,
    edited BOOLEAN DEFAULT FALSE
);

CREATE TABLE topic_votes (
    article_id INT NOT NULL REFERENCES article(id) ON DELETE CASCADE,
    topic_id INT NOT NULL REFERENCES topic(id) ON DELETE CASCADE,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    votes INT NOT NULL DEFAULT 0,
    PRIMARY KEY (article_id, topic_id)
);

CREATE TABLE change_topic (
    users_id INT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    article_id INT NOT NULL REFERENCES article(id) ON DELETE CASCADE,
    topic_id INT NOT NULL REFERENCES topic(id) ON DELETE CASCADE,
    added BOOLEAN NOT NULL DEFAULT TRUE,
    PRIMARY KEY (users_id, article_id, topic_id)
);

CREATE TABLE view_article (
    users_id INT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    article_id INT NOT NULL REFERENCES article(id) ON DELETE CASCADE,
    PRIMARY KEY (users_id, article_id)
);

CREATE TABLE view_comment (
    users_id INT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    comment_id INT NOT NULL REFERENCES comment(id) ON DELETE CASCADE,
    PRIMARY KEY (users_id, comment_id)
);

CREATE TABLE vote_article (
    users_id INT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    article_id INT NOT NULL REFERENCES article(id) ON DELETE CASCADE,
    PRIMARY KEY (users_id, article_id)
);

CREATE TABLE vote_comment (
    users_id INT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    comment_id INT NOT NULL REFERENCES comment(id) ON DELETE CASCADE,
    PRIMARY KEY (users_id, comment_id)
);

CREATE TABLE save_article (
    users_id INT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    article_id INT NOT NULL REFERENCES article(id) ON DELETE CASCADE,
    PRIMARY KEY (users_id, article_id)
);

CREATE TABLE save_comment (
    users_id INT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    comment_id INT NOT NULL REFERENCES comment(id) ON DELETE CASCADE,
    PRIMARY KEY (users_id, comment_id)
);

CREATE TABLE filter_topic  (
    users_id INT REFERENCES users(id) ON DELETE CASCADE,
    topic_id INT REFERENCES topic(id) ON DELETE CASCADE,
    blocked BOOLEAN DEFAULT FALSE,
    allowed BOOLEAN DEFAULT FALSE,
    max INT DEFAULT NULL,
    PRIMARY KEY (users_id, topic_id)
);

CREATE TABLE private_message (
    sender_users_id INT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    reciever_users_id INT NOT NULL REFERENCES users(id) ON DELETE CASCADE, 
    message TEXT NOT NULL,
    sended_on TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    PRIMARY KEY (sender_users_id, reciever_users_id)
);

CREATE TABLE block_user (
    users_id INT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    blocked_users_id INT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    block_reason_id INT NOT NULL REFERENCES article(id) ON DELETE CASCADE,
    article_id INT REFERENCES article(id) ON UPDATE CASCADE ON DELETE CASCADE CHECK (comment_id IS NULL),
    comment_id INT REFERENCES comment(id) ON UPDATE CASCADE ON DELETE CASCADE CHECK (article_id IS NULL),
    other_reason TEXT,
    PRIMARY KEY (users_id, blocked_users_id)
);

CREATE TABLE block_reason (
	id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    reason VARCHAR ( 255 ) NOT NULL,
    count INT NOT NULL DEFAULT 0
);


CREATE FUNCTION runtime(stamp timestamp with time zone) RETURNS integer AS $$
    SELECT extract(epoch from stamp - '2021-02-02');
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION exp_decay(votes integer, seconds integer, halflife integer) RETURNS real AS $$
    SELECT (ln(votes)/ln(2) + seconds/halflife);
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION conf_int_65(votes integer, views integer) RETURNS real AS $$
    SELECT ((votes + 0.4367 - 0.9346 * SQRT(votes - votes*votes/views + 0.2184)) / (views + 0.8735));
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION conf_int_80(votes integer, views integer) RETURNS real AS $$
    SELECT ((votes + 0.8212 - 1.2816 * SQRT(votes - votes*votes/views + 0.411)) / (views + 1.6424));
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION conf_int_95(votes integer, views integer) RETURNS real AS $$
    SELECT ((votes + 1.9207 - 1.96 * SQRT(votes - votes*votes/views + 0.9604)) / (views + 3.8415));
$$ LANGUAGE SQL IMMUTABLE;


CREATE INDEX article_votes_idx ON article(votes);
CREATE INDEX article_created_on_idx ON article(created_on);
CREATE INDEX article_exp_decay_1_idx ON article(exp_decay(votes, runtime(created_on), 3600));
CREATE INDEX article_exp_decay_2_idx ON article(exp_decay(votes, runtime(created_on), 3600*2));
CREATE INDEX article_exp_decay_5_idx ON article(exp_decay(votes, runtime(created_on), 3600*5));
CREATE INDEX article_exp_decay_10_idx ON article(exp_decay(votes, runtime(created_on), 3600*10));
CREATE INDEX article_exp_decay_20_idx ON article(exp_decay(votes, runtime(created_on), 3600*20));

CREATE INDEX comment_votes_idx ON comment(votes);
CREATE INDEX comment_created_on_idx ON comment(created_on);
CREATE INDEX comment_conf_int_65_idx ON comment(conf_int_65(votes, views));
CREATE INDEX comment_conf_int_80_idx ON comment(conf_int_80(votes, views));
CREATE INDEX comment_conf_int_95_idx ON comment(conf_int_95(votes, views));
