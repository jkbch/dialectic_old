package main

import (
	"context"
	"dialectic/internal/http"
	"dialectic/internal/postgres"
	"dialectic/internal/wikipedia"
	"log"
)

func main() {
	db, err := postgres.Open("localhost", 5432, "jakob", "dialectic")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	ctx := context.Background()

	ts := &postgres.TopicService{DB: db, CTX: ctx}

	req := http.NewRequestor(10)
	wiki := wikipedia.NewWikiPageService(req, &wikipedia.WikiConfig{
		BannedTiltes:     []string{"Main Page"},
		BannedCategories: []string{"Disambiguation pages"},
		MinViews:         10,
		ViewsDayLimit:    10,
		ValidNamespace:   0,
		SearchLimit:      10,
		DecriptionLimit:  5,
	})

	html := http.NewHtml("web/tmpl", "base", []string{"topicnew", "topicnewsearch", "t"})

	env := &http.Env{html, wiki, ts}

	log.Fatal(http.StartServer(env, 8090))
}
