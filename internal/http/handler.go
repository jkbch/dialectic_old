package http

import (
	"dialectic"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

type topicNew struct {
	AddedTopic bool
	Topic      *dialectic.Topic
}

func topicNewHandler(env *Env, w http.ResponseWriter, r *http.Request) error {
	return env.Html.Apply(w, "topicnew", topicNew{false, nil})
}

func topicNewPOSTHandler(env *Env, w http.ResponseWriter, r *http.Request) error {
	if err := r.ParseForm(); err != nil {
		return err
	}

	idStr := r.FormValue("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return StatusError{400, err}
	}

	page, err := env.Wiki.Page(id)
	if err != nil {
		return StatusError{500, err}
	}

	isBanned, _ := env.Wiki.Banned(page)
	if isBanned {
		return StatusError{400, fmt.Errorf("banned page id: %s", id)}
	}

	topic, err := env.TopicSvc.NewTopic(page)
	if err != nil {
		return err
	}

	return env.Html.Apply(w, "topicnew", topicNew{true, topic})
}

type topicNewSearch struct {
	Query     string
	WikiPages []*dialectic.WikiPage
}

func topicNewSearchHandler(env *Env, w http.ResponseWriter, r *http.Request) error {
	if err := r.ParseForm(); err != nil {
		return StatusError{400, err}
	}

	query := r.FormValue("query")
	if query == "" {
		http.Redirect(w, r, "/topic/new", http.StatusSeeOther)
		return nil
	}

	pages, err := env.Wiki.Search(query)
	if err != nil {
		return StatusError{400, err}
	}

	return env.Html.Apply(w, "topicnewsearch", topicNewSearch{query, pages})
}

type t struct {
	Title    string
	Topics   []*dialectic.Topic
	Articles []*dialectic.Article
}

func tHandler(env *Env, w http.ResponseWriter, r *http.Request) error {
	vars := mux.Vars(r)

	topicNamesURL, ok := vars["topics"]
	if !ok {
		return StatusError{404, nil}
	}

	topicNames := strings.Split(topicNamesURL, "|")
	topics, err := env.TopicSvc.TopicsByName(topicNames)
	if err != nil {
		return StatusError{500, err}
	}

	newTopicNames := make([]string, len(topics))
	for i, topic := range topics {
		newTopicNames[i] = strings.ReplaceAll(topic.Name, " ", "_")
	}
	newTopicNameURL := strings.Join(newTopicNames, "|")

	if newTopicNameURL != topicNamesURL {
		r.URL.Path = "/t/" + newTopicNameURL
		http.Redirect(w, r, r.URL.RequestURI(), 303)
		return nil
	}

	if err := r.ParseForm(); err != nil {
		return StatusError{400, err}
	}

	limit := 30
	offset := 0
	if r.FormValue("offset") != "" {
		offset, err = strconv.Atoi(r.FormValue("offset"))
		if err != nil {
			offset = 0
		}
	}

	var articles []*dialectic.Article
	switch r.FormValue("ranking") {
	case "exp_decay":
		halfLife := 10
		switch r.FormValue("half-life") {
		case "1":
			halfLife = 1
		case "2":
			halfLife = 2
		case "5":
			halfLife = 5
		case "10":
			halfLife = 10
		case "20":
			halfLife = 20
		}

		articles, err = env.TopicSvc.TopicsExpDecayArticles(topics, halfLife, offset, limit)

	case "newest":
		articles, err = env.TopicSvc.TopicsNewestArticles(topics, offset, limit)

	case "most_voted":
		timePeriod := strings.ReplaceAll(r.FormValue("time_period"), "_", " ")

		articles, err = env.TopicSvc.TopicsMostVotedArticles(topics, timePeriod, offset, limit)

	default:
		halfLife := 10
		articles, err = env.TopicSvc.TopicsExpDecayArticles(topics, halfLife, offset, limit)
	}

	if err != nil {
		return StatusError{500, err}
	}

	title := strings.ReplaceAll(newTopicNameURL, "_", " ") + " - Dialectic"

	return env.Html.Apply(w, "t", t{title, topics, articles})
}
