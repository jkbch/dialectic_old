package http

import (
	"dialectic"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type Error interface {
	error
	Status() int
}

type StatusError struct {
	Code int
	Err  error
}

func (se StatusError) Error() string {
	return se.Err.Error()
}

func (se StatusError) Status() int {
	return se.Code
}

type Env struct {
	Html     *Html
	Wiki     dialectic.WikiPageService
	TopicSvc dialectic.TopicService
	// userSvc    *dialectic.UserService
	// postSvc    *dialectic.PostService
	// articleSvc *dialectic.ArticleService
}

type Handler struct {
	*Env
	H func(e *Env, w http.ResponseWriter, r *http.Request) error
}

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := h.H(h.Env, w, r)
	if err != nil {
		switch e := err.(type) {
		case Error:
			log.Printf("HTTP %d - %s", e.Status(), e)
			http.Error(w, e.Error(), e.Status())
		default:
			http.Error(w, http.StatusText(http.StatusInternalServerError),
				http.StatusInternalServerError)
		}
	}
}

func StartServer(env *Env, port int) error {
	r := mux.NewRouter()
	r.Handle("/t/{topics}", Handler{env, tHandler})
	r.Handle("/article/new", Handler{env, tHandler})
	r.Handle("/topic/new", Handler{env, topicNewPOSTHandler}).Methods("POST")
	r.Handle("/topic/new", Handler{env, topicNewHandler})
	r.Handle("/topic/new/search", Handler{env, topicNewSearchHandler})
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))

	fmt.Println("Server started on port " + strconv.Itoa(port))
	return http.ListenAndServe(":"+strconv.Itoa(port), r)
}
