package http

import (
	"fmt"
	"net/http"
	"path"
	"text/template"
)

type Html struct {
	baseTemplate string
	tmplMap      map[string]*template.Template
}

func NewHtml(templatePath string, baseTemplate string, subTemplates []string) *Html {
	var tmpl = make(map[string]*template.Template)

	baseTemplatePath := path.Join(templatePath, baseTemplate+".html")

	for _, v := range subTemplates {
		subTemplatePath := path.Join(templatePath, v+".html")
		tmpl[v] = template.Must(template.ParseFiles(subTemplatePath, baseTemplatePath))
	}

	return &Html{baseTemplate, tmpl}
}

func (t *Html) Apply(w http.ResponseWriter, name string, data interface{}) (err error) {
	val, ok := t.tmplMap[name]

	if !ok {
		err = fmt.Errorf("Template name does not exists")
		return
	}

	val.ExecuteTemplate(w, t.baseTemplate, data)
	return
}
