package http

import (
	"dialectic"
	"encoding/json"
	"net/http"
	"time"
)

type Requestor struct {
	client *http.Client
}

func NewRequestor(timeoutSeconds int) dialectic.Requestor {
	client := &http.Client{Timeout: 10 * time.Second}
	return &Requestor{client}
}

func (r *Requestor) GetJSON(url string, target interface{}) error {
	res, err := r.client.Get(url)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	return json.NewDecoder(res.Body).Decode(target)
}
