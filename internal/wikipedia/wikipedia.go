package wikipedia

import (
	"dialectic"
	"fmt"
	"net/url"
)

type WikiConfig struct {
	BannedTiltes     []string
	BannedCategories []string
	MinViews         int
	ViewsDayLimit    int
	ValidNamespace   int
	SearchLimit      int
	DecriptionLimit  int
}

type WikiPageService struct {
	req dialectic.Requestor
	WikiConfig
}

func NewWikiPageService(req dialectic.Requestor, cfg *WikiConfig) dialectic.WikiPageService {
	return &WikiPageService{req, *cfg}
}

func (w *WikiPageService) makeCategoryQuery() (categoryQuery string, err error) {
	if len(w.BannedCategories) == 0 {
		err = fmt.Errorf("Wikiepdia banned categories must not be empty")
		return
	}

	categoryQuery = ""
	for _, title := range w.BannedCategories {
		categoryQuery += "Category:" + title + "|"
	}
	categoryQuery = url.QueryEscape(categoryQuery[:len(categoryQuery)-1])

	return
}

func (w *WikiPageService) Banned(page *dialectic.WikiPage) (isBanned bool, banMessages []string) {
	isTitleBanned := false
	for _, bannedTilte := range w.BannedTiltes {
		if page.Title == bannedTilte {
			isTitleBanned = true
		}
	}
	if isTitleBanned {
		banMessages = append(banMessages, "Page title is banned")
	}
	if page.TotalViews < w.MinViews {
		banMessages = append(banMessages, "Page does not have enough views")
	}
	if page.Namespace != w.ValidNamespace {
		banMessages = append(banMessages, "Page is in invalid namespace")
	}
	if len(page.Categories) != 0 {
		banMessages = append(banMessages, "Page has a banned category")
	}
	isBanned = len(banMessages) != 0
	return
}

func (w *WikiPageService) Page(id int) (page *dialectic.WikiPage, err error) {
	categoryQuery, err := w.makeCategoryQuery()
	if err != nil {
		return
	}

	wikipediaURL := fmt.Sprintf(
		"https://en.wikipedia.org/w/api.php?action=query&format=json"+
			"&redirects=1&pageids=%v"+
			"&prop=extracts|categories|pageviews"+
			"&exintro=1&explaintext=1&exsentences=%v"+
			"&clshow=!hidden&cllimit=max&clcategories=%v"+
			"&pvipdays=%v",
		id, w.DecriptionLimit, categoryQuery, w.ViewsDayLimit)

	res := &jsonResponse{}
	if err = w.req.GetJSON(wikipediaURL, res); err != nil {
		return
	}

	_, doesPageNotExist := res.Query.Pages["-1"]
	if doesPageNotExist {
		err = fmt.Errorf("No Wikipedia page with id: %v", id)
		return
	}

	for _, v := range res.Query.Pages {
		page = v.toPage()
	}

	return
}

func (w *WikiPageService) Search(query string) (pages []*dialectic.WikiPage, err error) {
	categoryQuery, err := w.makeCategoryQuery()
	if err != nil {
		return
	}

	safeQuery := url.QueryEscape(query)
	wikipediaURL := fmt.Sprintf(
		"https://en.wikipedia.org/w/api.php?action=query&format=json&redirects=1"+
			"&generator=prefixsearch&gpssearch=%v&gpsnamespace=%v&gpslimit=%v&"+
			"prop=extracts|categories|pageviews"+
			"&exintro=1&explaintext=1&exsentences=%v"+
			"&clshow=!hidden&cllimit=max&clcategories=%v"+
			"&pvipdays=%v",
		safeQuery, w.ValidNamespace, w.SearchLimit, w.DecriptionLimit, categoryQuery, w.ViewsDayLimit)

	res := &jsonResponse{}
	if err = w.req.GetJSON(wikipediaURL, res); err != nil {
		return
	}

	incompleteResponse := false
	for _, pageJSON := range res.Query.Pages {
		if len(pageJSON.Pageviews) == 0 {
			incompleteResponse = true
		}
	}
	if incompleteResponse {
		if err = w.req.GetJSON(wikipediaURL, res); err != nil {
			return
		}
	}

	pages = make([]*dialectic.WikiPage, len(res.Query.Pages))
	for _, JSONPage := range res.Query.Pages {
		pages[JSONPage.Index-1] = JSONPage.toPage()
	}

	return
}

type jsonResponse struct {
	Query struct {
		Pages map[string]jsonPage `json:"pages"`
	} `json:"query"`
}

type jsonPage struct {
	PageID     int    `json:"pageid"`
	Namespace  int    `json:"ns"`
	Title      string `json:"title"`
	Index      int    `json:"index"`
	Extract    string `json:"extract"`
	Categories []struct {
		Namespace int    `json:"ns"`
		Title     string `json:"title"`
	} `json:"categories"`
	Pageviews map[string]int `json:"pageviews"`
}

func (p *jsonPage) toPage() *dialectic.WikiPage {
	totalViews := 0
	for _, views := range p.Pageviews {
		totalViews += views
	}
	categories := make([]string, len(p.Categories))
	for i, categoryJSON := range p.Categories {
		categories[i] = categoryJSON.Title
	}

	return &dialectic.WikiPage{
		WikipediaID: p.PageID,
		Title:       p.Title,
		Extract:     p.Extract,
		TotalViews:  totalViews,
		Namespace:   p.Namespace,
		Categories:  categories}
}
