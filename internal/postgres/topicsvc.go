package postgres

import (
	"context"
	"dialectic"
	"strconv"
	"strings"

	"github.com/jackc/pgx/v4/pgxpool"
)

type TopicService struct {
	DB  *pgxpool.Pool
	CTX context.Context
}

func (t *TopicService) NewTopic(page *dialectic.WikiPage) (topic *dialectic.Topic, err error) {
	sql := `
		INSERT INTO topic (name, extract, wikipedia_id) VALUES ($1, $2, $3) 
		RETURNING id, default_blocked, default_allowed
	`

	var id int
	var block bool
	var allow bool

	err = t.DB.QueryRow(t.CTX, sql, page.Title, page.Extract, page.WikipediaID).Scan(
		&id,
		&block,
		&allow,
	)
	if err != nil {
		return
	}

	topic = &dialectic.Topic{
		ID:             id,
		Name:           page.Title,
		Extract:        page.Extract,
		WikipediaID:    page.WikipediaID,
		DefaultBlocked: block,
		DefaultAllowed: allow,
	}

	return
}

func (t *TopicService) TopicsByName(topicNames []string) (topics []*dialectic.Topic, err error) {
	for i, v := range topicNames {
		topicNames[i] = strings.ReplaceAll(strings.ToLower(v), "_", " ")
	}

	sql := `
		SELECT id, name, extract, wikipedia_id, 
		default_blocked, default_allowed, created_on 
		FROM topic WHERE LOWER(name) = ANY ($1)
		ORDER BY name
	`

	rows, err := t.DB.Query(t.CTX, sql, topicNames)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var topic dialectic.Topic
		err = rows.Scan(
			&topic.ID,
			&topic.Name,
			&topic.Extract,
			&topic.WikipediaID,
			&topic.DefaultBlocked,
			&topic.DefaultAllowed,
			&topic.CreatedOn,
		)
		if err != nil {
			return
		}
		topics = append(topics, &topic)
	}

	err = rows.Err()
	return
}

func (t *TopicService) queryArticles(sql string, args []interface{}) (articles []*dialectic.Article, err error) {
	rows, err := t.DB.Query(t.CTX, sql, args...)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var article dialectic.Article
		err = rows.Scan(
			&article.ID,
			&article.UserName,
			&article.Title,
			&article.Content,
			&article.Votes,
			&article.Views,
			&article.CreatedOn,
			&article.ActiveTo,
			&article.TopicNames,
		)
		if err != nil {
			return
		}
		articles = append(articles, &article)
	}

	err = rows.Err()
	return
}

func (TopicService) appendTimePeriod(args *[]interface{}, whereSQLs *[]string, timePeriod string) {
	if !(timePeriod == "all time" || timePeriod == "") {
		*whereSQLs = append(*whereSQLs, "a.created_on < NOW() - INTERVAL $"+strconv.Itoa(len(*args)))
		*args = append(*args, timePeriod)
	}
}

func (TopicService) topicsArticlesSQL(orderBySQL string, whereSQLs []string) (sql string) {
	sql = `
		SELECT a.id, a.users_name, a.title, a.content, a.votes,
		a.views, a.created_on, a.active_to, ARRAY_AGG(t.name)
		FROM topic_votes AS tv
			INNER JOIN article AS a ON tv.article_id = a.id
			INNER JOIN topic AS t ON tv.topic_id = t.id
		WHERE tv.active
		` + joinWhereSQLs(whereSQLs) + `
		GROUP BY a.id HAVING $1 <@ ARRAY_AGG(t.id)
		ORDER BY ` + orderBySQL + ` DESC LIMIT $2 OFFSET $3
	`
	return
}

func (TopicService) topicsWhereSQLs() (whereSQLs []string) {
	whereSQLs = append(whereSQLs, "NOT a.hidden")
	return
}

func (t *TopicService) TopicsExpDecayArticles(topics []*dialectic.Topic, halfLife int, limit int, offset int) (articles []*dialectic.Article, err error) {
	args := []interface{}{extractTopicIds(topics), limit, offset, halfLife}

	orderBySQL := "exp_decay(a.votes, runtime(a.created_on), $4)"
	whereSQLs := t.topicsWhereSQLs()

	sql := t.topicsArticlesSQL(orderBySQL, whereSQLs)

	articles, err = t.queryArticles(sql, args)
	return
}

func (t *TopicService) TopicsNewestArticles(topics []*dialectic.Topic, limit int, offset int) (articles []*dialectic.Article, err error) {
	args := []interface{}{extractTopicIds(topics), limit, offset}

	orderBySQL := "a.created_on"
	whereSQLs := t.topicsWhereSQLs()

	sql := t.topicsArticlesSQL(orderBySQL, whereSQLs)

	articles, err = t.queryArticles(sql, args)
	return
}

func (t *TopicService) TopicsMostVotedArticles(topics []*dialectic.Topic, timePeriod string, limit int, offset int) (articles []*dialectic.Article, err error) {
	args := []interface{}{extractTopicIds(topics), limit, offset}

	orderBySQL := "a.votes"
	whereSQLs := t.topicsWhereSQLs()

	t.appendTimePeriod(&args, &whereSQLs, timePeriod)

	sql := t.topicsArticlesSQL(orderBySQL, whereSQLs)

	articles, err = t.queryArticles(sql, args)
	return
}

func (TopicService) userTopicsArticlesSQL(orderBySQL string, whereSQLs []string) (sql string) {
	sql = `
		SELECT a.id, a.users_name, a.title, a.content, a.votes,
		a.views, a.created_on, a.active_to, ARRAY_AGG(t.name)
		FROM topic_votes AS tv
			FULL JOIN change_topic AS ct ON tv.article_id = ct.article_id AND tv.topic_id = ct.topic_id AND ct.users_id = $1
			INNER JOIN article AS a ON COALESCE(tv.article_id, ct.article_id) = a.id
			INNER JOIN topic AS t ON COALESCE(tv.topic_id, ct.article_id) = t.id
			LEFT JOIN block_user AS bu ON a.users_id = bu.blocked_users_id AND bu.users_id = $1
		WHERE COALESCE(ct.added, tv.active) AND bu.blocked_users_id IS NULL
		` + joinWhereSQLs(whereSQLs) + `
		GROUP BY a.id HAVING $2 <@ ARRAY_AGG(t.id)
		ORDER BY ` + orderBySQL + ` DESC LIMIT $3 OFFSET $4
	`
	return
}

func (TopicService) userTopicsWhereSQLs(user *dialectic.User) (whereSQLs []string) {
	if !user.ShowHidden {
		whereSQLs = append(whereSQLs, "NOT a.hidden")
	}
	return
}

func (t *TopicService) UserTopicsExpDecayArticles(user *dialectic.User, topics []*dialectic.Topic, halfLife int, limit int, offset int) (articles []*dialectic.Article, err error) {
	args := []interface{}{user.ID, extractTopicIds(topics), limit, offset, halfLife}

	orderBySQL := "exp_decay(a.votes, runtime(a.created_on), $5)"
	whereSQLs := t.userTopicsWhereSQLs(user)

	sql := t.userTopicsArticlesSQL(orderBySQL, whereSQLs)

	articles, err = t.queryArticles(sql, args)
	return
}

func (t *TopicService) UserTopicsNewestArticles(user *dialectic.User, topics []*dialectic.Topic, limit int, offset int) (articles []*dialectic.Article, err error) {
	args := []interface{}{user.ID, extractTopicIds(topics), limit, offset}

	orderBySQL := "a.created_on"
	whereSQLs := t.userTopicsWhereSQLs(user)

	sql := t.userTopicsArticlesSQL(orderBySQL, whereSQLs)

	articles, err = t.queryArticles(sql, args)
	return
}

func (t *TopicService) UserTopicsMostVotedArticles(user *dialectic.User, topics []*dialectic.Topic, timePeriod string, limit int, offset int) (articles []*dialectic.Article, err error) {
	args := []interface{}{user.ID, extractTopicIds(topics), limit, offset}

	orderBySQL := "a.votes"
	whereSQLs := t.userTopicsWhereSQLs(user)

	t.appendTimePeriod(&args, &whereSQLs, timePeriod)

	sql := t.userTopicsArticlesSQL(orderBySQL, whereSQLs)

	articles, err = t.queryArticles(sql, args)
	return
}

func (TopicService) homeArticlesSQL(orderBySQL string, whereSQLs []string) (sql string) {
	sql = `
		SELECT a.id, a.users_name, a.title, a.content, a.votes,
		a.views, a.created_on, a.active_to, ARRAY_AGG(t.name)
		FROM topic_votes AS tv
			FULL JOIN change_topic AS ct ON tv.article_id = ct.article_id AND tv.topic_id = ct.topic_id AND ct.users_id = $1
			INNER JOIN topic AS t ON COALESCE(tv.topic_id, ct.topic_id) = t.id
			LEFT JOIN filter_topic AS ft ON t.id = ft.topic_id AND ft.users_id = $1
			INNER JOIN article AS a ON COALESCE(tv.article_id, ct.article_id) = a.id
			LEFT JOIN block_user AS bu ON a.users_id = bu.blocked_users_id AND bu.users_id = $1
		WHERE COALESCE(ct.added, tv.active) AND bu.blocked_users_id IS NULL
		` + joinWhereSQLs(whereSQLs) + `
		GROUP BY a.id ORDER BY ` + orderBySQL + ` DESC LIMIT $2 OFFSET $3;
	`
	return
}

func (TopicService) homeWhereSQLs(user *dialectic.User) (whereSQLs []string) {
	if !user.ShowHidden {
		whereSQLs = append(whereSQLs, "NOT a.hidden")
	}
	if user.BlockMode {
		whereSQLs = append(whereSQLs, "NOT COALESCE(ft.blocked, t.default_blocked)")
	}
	if user.AllowMode {
		whereSQLs = append(whereSQLs, "COALESCE(ft.allowed, t.default_allowed)")
	}
	return
}

func (t *TopicService) HomeExpDecayArticles(user *dialectic.User, halfLife int, limit int, offset int) (articles []*dialectic.Article, err error) {
	args := []interface{}{user.ID, limit, offset, halfLife}

	orderBySQL := "exp_decay(a.votes, runtime(a.created_on), $4)"
	whereSQLs := t.homeWhereSQLs(user)

	sql := t.homeArticlesSQL(orderBySQL, whereSQLs)

	articles, err = t.queryArticles(sql, args)
	return
}

func (t *TopicService) HomeNewestArticles(user *dialectic.User, limit int, offset int) (articles []*dialectic.Article, err error) {
	args := []interface{}{user.ID, limit, offset}

	orderBySQL := "a.created_on"
	whereSQLs := t.homeWhereSQLs(user)

	sql := t.homeArticlesSQL(orderBySQL, whereSQLs)

	articles, err = t.queryArticles(sql, args)
	return
}

func (t *TopicService) HomeMostVotedArticles(user *dialectic.User, timePeriod string, limit int, offset int) (articles []*dialectic.Article, err error) {
	args := []interface{}{user.ID, limit, offset}

	orderBySQL := "a.votes"
	whereSQLs := t.homeWhereSQLs(user)

	t.appendTimePeriod(&args, &whereSQLs, timePeriod)

	sql := t.homeArticlesSQL(orderBySQL, whereSQLs)

	articles, err = t.queryArticles(sql, args)
	return
}
