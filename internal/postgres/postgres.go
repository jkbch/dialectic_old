package postgres

import (
	"context"
	"dialectic"
	"fmt"
	"strings"

	"github.com/jackc/pgx/v4/pgxpool"
)

func Open(host string, port int, user string, dbname string) (db *pgxpool.Pool, err error) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s dbname=%s sslmode=disable",
		host, port, user, dbname)

	db, err = pgxpool.Connect(context.Background(), psqlInfo)
	if err != nil {
		return
	}

	var greeting string
	err = db.QueryRow(context.Background(), "select 'Database up'").Scan(&greeting)
	if err != nil {
		return
	}
	fmt.Println(greeting)
	return
}

func extractTopicIds(topics []*dialectic.Topic) (ids []int) {
	ids = make([]int, len(topics))
	for i, topic := range topics {
		ids[i] = topic.ID
	}
	return
}

func joinWhereSQLs(whereSQLs []string) string {
	if len(whereSQLs) == 0 {
		return ""
	}
	return "AND " + strings.Join(whereSQLs, " AND ")
}
